﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImportBP.Models;

namespace ImportBP.Fliters
{
    public sealed class BpFliter
    {
        public BpFuelTransaction.FuelType FuelType { get; set; }
        public string CarRegistration { get; set; }
        public string PetrolStationLocation { get; set; }
        public string Customer { get; set; }
        public string CostCenter { get; set; }

        public bool CanFliter() => typeof(BpFliter).GetProperties().Any(property => !string.IsNullOrEmpty(property.GetValue(typeof(BpFliter)).ToString()));
    }
}
