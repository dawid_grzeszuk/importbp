﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportBP.Models
{
    public sealed class BpFuelTransaction
    {
        public sealed class Root
        {
            public Client Client { get; set; }
            public Car Car { get; set; }
            public PetrolStation PetrolStation { get; set; }
            public PreviousFuelRefill PreviousFuelRefill { get; set; }
            public FuelInvoice FuelInvoice { get; set; }

            public Root()
            {
                Client = new Client();
                Car = new Car();
                PetrolStation = new PetrolStation();
                PreviousFuelRefill = new PreviousFuelRefill();
                FuelInvoice = new FuelInvoice();
            }
        }

        public sealed class Client
        {
            public string PrimaryClientName { get; set; }
            public string SecondaryClientName { get; set; }
            public int CardNumber { get; set; }
            public string CostCenter { get; set; }
        }

        public sealed class Car
        {
            public string CarRegistration { get; set; }
            public int Odometer { get; set; }
            public double FuelUsagePer100Km { get; set; }
            public double KilometersFromOneLitre { get; set; }
            public double NettoPriceOfOneKilometer { get; set; }
        }

        public sealed class PetrolStation
        {
            public string Brand { get; } = "BP";
            public string Localization { get; set; }
        }

        public sealed class PreviousFuelRefill
        {
            public double KilometersFromPreviousRefill { get; set; }
            public DateTime TransactionDateTime { get; set; }
        }

        public sealed class FuelInvoice
        {
            public FuelType FuelType { get; set; }
            public double Quantity { get; set; }
            public double NettoPriceOnInvoice { get; set; }
            public double BruttoPriceOnInvoice { get; set; }
            public double VatOnInvoice { get; set; }
            public double NettoPriceOfOneLiter { get; set; }
        }

        public enum FuelType
        {
            On = 0 , OnUltimate = 1 , Pb95 = 2 , Pb98 = 3 , Pb98Ultimate , Lpg = 5
        }
    }
}
