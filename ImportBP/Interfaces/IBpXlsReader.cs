﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImportBP.Fliters;
using ImportBP.Models;
using OfficeOpenXml;

namespace ImportBP.Interfaces
{
    public interface IBpXlsReader
    {
        ExcelPackage Read(string path);
        IEnumerable<BpFuelTransaction.Root> MapToObject(ExcelPackage file);
        IEnumerable<BpFuelTransaction.Root> Fliter(IEnumerable<BpFuelTransaction.Root> transactions, BpFliter fliter);
        bool SendToEnova();
    }
}
