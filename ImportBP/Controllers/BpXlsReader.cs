﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ImportBP.Fliters;
using ImportBP.Interfaces;
using ImportBP.Models;
using OfficeOpenXml;

namespace ImportBP.Controllers
{
    public class BpXlsReader : IBpXlsReader
    {
        #region File Loading
        //Sprawdzamy czy ścieżka nie jest pusta i czy prowadzi do pliku
        public ExcelPackage Read(string path) => File.Exists(path) ? new ExcelPackage(File.OpenRead(path)) : null;
        #endregion

        #region File Reading
        public IEnumerable<BpFuelTransaction.Root> MapToObject(ExcelPackage file)
        {
            //Jeżeli nie ma pliku kończymy działanie funkcji
            if (Equals(file, null)) return null;

            //Tworzymy liste transakcji
            var transactions = new List<BpFuelTransaction.Root>();

            using (var worksheet = file.Workbook.Worksheets[1])
            {
                var tempTranscation = new BpFuelTransaction.Root();

                for (var row = 7; row < worksheet.Dimension.Rows + 1; row++)
                {
                    for (var column = 1; column < worksheet.Dimension.Columns + 1; column++)
                    {
                        if (Equals(worksheet.Cells[row, column].Value, null)) continue;
                        if (string.IsNullOrEmpty(worksheet.Cells[row, column].Value.ToString())) continue;
                        if (worksheet.Cells[row, column].Value.ToString().ToLower().Replace('ó', 'o').Replace('ł', 'l').Contains("ogolem")) continue;

                        switch (column)
                        {
                            case 1:
                                tempTranscation.Client.PrimaryClientName = worksheet.Cells[row, column].Value.ToString();
                                break;
                            case 2:
                                tempTranscation.Client.CostCenter = worksheet.Cells[row, column].Value.ToString();
                                break;
                            case 3:
                                tempTranscation.Client.CardNumber = Convert.ToInt32(worksheet.Cells[row, column].Value);
                                break;
                            case 4:
                            {
                                var value = worksheet.Cells[row, column].Value.ToString().Split('/');
                                tempTranscation.Car.CarRegistration = value[0];
                                tempTranscation.Client.SecondaryClientName = value[1];
                            }
                                break;
                            case 5:
                                tempTranscation.Car.FuelUsagePer100Km = Convert.ToDouble(worksheet.Cells[row, column].Value);
                                break;
                            case 6:
                                tempTranscation.Car.KilometersFromOneLitre = Convert.ToDouble(worksheet.Cells[row, column].Value);
                                break;
                            case 7:
                                tempTranscation.Car.NettoPriceOfOneKilometer = Convert.ToDouble(worksheet.Cells[row, column].Value);
                                break;
                            case 8:
                                tempTranscation.Car.Odometer = Convert.ToInt32(worksheet.Cells[row, column].Value);
                                break;
                            case 9:
                                tempTranscation.PreviousFuelRefill.KilometersFromPreviousRefill = Convert.ToDouble(worksheet.Cells[row, column].Value);
                                break;
                            case 10:
                                tempTranscation.PreviousFuelRefill.TransactionDateTime = DateTime.FromOADate(double.Parse(worksheet.Cells[row, column].Value.ToString()));
                                break;
                            case 11:
                                tempTranscation.PetrolStation.Brand = worksheet.Cells[row, column].Value.ToString();
                                break;
                            case 12:
                                tempTranscation.PetrolStation.Localization = worksheet.Cells[row, column].Value.ToString();
                                break;
                            case 13:
                            {
                                var value = worksheet.Cells[row, column].Value.ToString().ToLower();

                                if (value.Contains("ultimate"))
                                    tempTranscation.FuelInvoice.FuelType = value.Contains("98") ? BpFuelTransaction.FuelType.Pb98Ultimate : BpFuelTransaction.FuelType.OnUltimate;

                                else if (value.Contains("olej")) tempTranscation.FuelInvoice.FuelType = BpFuelTransaction.FuelType.On;
                                else if (value.Contains("95")) tempTranscation.FuelInvoice.FuelType = BpFuelTransaction.FuelType.Pb95;
                                else if (value.Contains("98")) tempTranscation.FuelInvoice.FuelType = BpFuelTransaction.FuelType.Pb98;
                                else if (value.Contains("gaz")) tempTranscation.FuelInvoice.FuelType = BpFuelTransaction.FuelType.Lpg;
                            }
                                break;
                            case 14:
                                tempTranscation.FuelInvoice.Quantity = Convert.ToDouble(worksheet.Cells[row, column].Value);
                                break;
                            case 16:
                                tempTranscation.FuelInvoice.NettoPriceOnInvoice = Convert.ToDouble(worksheet.Cells[row, column].Value);
                                break;
                            case 17:
                                tempTranscation.FuelInvoice.VatOnInvoice = Convert.ToDouble(worksheet.Cells[row, column].Value);
                                break;
                            case 18:
                                tempTranscation.FuelInvoice.BruttoPriceOnInvoice = Convert.ToDouble(worksheet.Cells[row, column].Value);
                                break;
                            case 19:
                                tempTranscation.FuelInvoice.NettoPriceOfOneLiter = Convert.ToDouble(worksheet.Cells[row, column].Value);
                                break;
                            default:
                                break;
                        }
                    }
                }
                transactions.Add(tempTranscation);
            }
            Console.ReadLine();
            return transactions;
        }
        #endregion

        #region File Flitering
        public IEnumerable<BpFuelTransaction.Root> Fliter(IEnumerable<BpFuelTransaction.Root> transactions, BpFliter fliter)
        {
            var enumerable = transactions as BpFuelTransaction.Root[] ?? transactions.ToArray();

            if (!enumerable.Any()) return null;
            if (!fliter.CanFliter()) return enumerable;

            var fliterList = enumerable.ToList();

            foreach (var transaction in fliterList)
            {
                if (!string.IsNullOrEmpty(fliter.Customer) && fliter.Customer != transaction.Client.PrimaryClientName)
                {
                    fliterList.Remove(transaction);
                }

                if (!string.IsNullOrEmpty(fliter.CarRegistration) && fliter.CarRegistration != transaction.Car.CarRegistration)
                {
                    fliterList.Remove(transaction);
                }

                if (!string.IsNullOrEmpty(fliter.CostCenter) && fliter.CostCenter != transaction.Client.CostCenter)
                {
                    fliterList.Remove(transaction);
                }

                if (!string.IsNullOrEmpty(fliter.PetrolStationLocation) && fliter.PetrolStationLocation != transaction.PetrolStation.Localization)
                {
                    fliterList.Remove(transaction);
                }

                if (!string.IsNullOrEmpty(fliter.FuelType.ToString()) && fliter.FuelType != transaction.FuelInvoice.FuelType)
                {
                    fliterList.Remove(transaction);
                }
            }
            return fliterList;
        }
        #endregion

        #region File Sending to Enova
        public bool SendToEnova()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}