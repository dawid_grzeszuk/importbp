﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImportBP.Controllers;

namespace ImportBP
{
    class Program
    {
        static void Main(string[] args)
        {
            var access = new BpXlsReader();
            access.MapToObject(access.Read("Paliwo.xlsx"));
        }
    }
}
